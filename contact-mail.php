<?php
header("Content-type: Application/json");
$result = ["status" => "ok"];
if(!$_POST['name'] || !$_POST['email'] || !$_POST['message']) {
    $result["status"] = "failure";
    $result["error"] = "Please provide valid details.";
} else {
    $emailTo = 'shafeequebodhi@gmail.com ';
    $name 		 = trim(ucfirst(strtolower($_POST['name'])));
    $email		 = trim($_POST['email']);
    //$subject1	 = trim($_POST['subject']);
    $message 	 = trim($_POST['message']);

    $subject	 = "Mail from bodhi website Contact page  ";
    if ($_POST['subject']) {
        $subject = trim($_POST['subject']);
    }
    // Send email
    $headers = "From: " . $name . " <" . $email . ">" . "\r\n" . "Reply-To: " . $email;
    $body 	 = "Name : ".$name."\r\nEmail : ".$email."\r\nSubject : ".$subject."\r\nMessage : ".$message."\r\n";
    $success = mail($emailTo, $subject, $body, $headers);
    if($success)
    {
        $result["success"] = "Your feedback has been submitted successfully.";
    }
    else
    {
        $result["status"] = "failure";
        $result["error"] = "Something went wrong! Please try again";
    }
}
echo json_encode($result);