$(function () {
    $(document).on('click', 'div[data-role="trigger_nav"]', function () {
        $('body').css({"height": $(window).height(), "overflow-y": "hidden"});
        $('nav').fadeIn();
        $('nav ul').addClass('active');
    });
    $(document).on('click', 'span[data-role="close_nav"]', function () {
        $('body').css({"height": "auto", "overflow-y": "visible"});
        $('nav').fadeOut();
        $('nav ul').removeClass('active');
    });
    $(document).on('click', '.nav_over_lay', function () {
        $('body').css({"height": "auto", "overflow-y": "visible"});
        $('nav').fadeOut();
        $('nav ul').removeClass('active');
    });
});
